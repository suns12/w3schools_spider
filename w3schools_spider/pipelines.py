# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import logging
from twisted.enterprise import adbapi
import pymysql.cursors
from scrapy.pipelines.images import ImagesPipeline
from urllib import parse
import time

class W3SchoolsSpiderPipeline(object):
    def process_item(self, item, spider):
        return item


class MysqlPipeline(object):

    def __init__(self):

        self.dbpool = adbapi.ConnectionPool(
            'pymysql',
            db='w3schools_spider',
            host='127.0.0.1',
            user='root',
            passwd='asdf@#123.',
            cursorclass=pymysql.cursors.DictCursor,
            charset='utf8',
            use_unicode=True)



    def process_item(self, item, spider):
        # run db query in thread pool
        query = self.dbpool.runInteraction(self._conditional_insert, item)
        query.addErrback(self.handle_error)

        return item

    def _conditional_insert(self, tx, item):
            sql = """INSERT INTO `tbl_posts` (`path`,`slug`,`categories`,`type`,`list_title`,`content_title`,`data`,`create_time`)
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"""
            create_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
            lis = (item['path'], item['slug'], item['categories'], item['type'], item['list_title'], item['content_title'], item['data'], create_time)
            tx.execute(sql, lis)


    def handle_error(self, e):
        print('handle_error')
        logging.error(e)


class MyImagesPipeline(ImagesPipeline):
    def file_path(self, request, response=None, info=None):
        image_guid = request.url.split('/')[-1]
        image_guid = parse.unquote(image_guid).decode('utf-8')
        return '%s' % (image_guid)

