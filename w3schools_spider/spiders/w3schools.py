# -*- coding: utf-8 -*-
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from w3schools_spider.items import W3SchoolsSpiderItem
from urllib.parse import urlparse

class W3schoolsSpider(CrawlSpider):
    name = 'w3schools'
    allowed_domains = ['www.w3schools.com']
    start_urls = ['https://www.w3schools.com']
    # 从页面需要提取的url 链接(link)
    links_tutorials = LinkExtractor(restrict_xpaths='//*[@id="nav_references"]/div')
    # 设置解析link的规则，callback是指解析link返回的响应数据的的方法
    rules = [
        Rule(LinkExtractor(restrict_xpaths='//div[@id="leftmenuinnerinner"]'), callback='parse_content'), # 爬取详情页，不follow
        Rule(link_extractor=links_tutorials, callback='parse_content', follow=True),  # 爬取首页，不follow
    ]

    # links_references = LinkExtractor(restrict_xpaths='//*[@id="nav_references"]/div/div')
    # 设置解析link的规则，callback是指解析link返回的响应数据的的方法
    # rules = [
    #     Rule(link_extractor=links_references, callback='parse_content', follow=True),  # 爬取首页，不follow
    #     Rule(LinkExtractor(restrict_xpaths='//div[@id="leftmenuinnerinner"]'), callback='parse_content'),  # 爬取详情页，不follow
    # ]


    def parse_content(self, response):
        Item = W3SchoolsSpiderItem()
        path = urlparse(response.url).path
        Item['path'] = path
        Item['slug'] = path.split('/')[1]
        Item['categories'] = 'tutorials'
        Item['type'] = 'references'
        Item['list_title'] = response.meta['link_text']
        Item['content_title'] = response.xpath('//div[@id="main"]/h1').xpath('string(.)').get()
        Item['data'] = response.xpath('//div[@id="main"]').get()
        yield Item





